
This repository is used for developing support of offscreen Android views for Qt applications.

Please note that the modules are published under different open-source licenses.

Contact: Sergey Galin <s.galin@2gis.ru>

TODO:

- Develop the Offscreen Views more ;-)
- Support "stock" Qt 5.2
- Add Qt 5.2 /QML example with stock Qt support

