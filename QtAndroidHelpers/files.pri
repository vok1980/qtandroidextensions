
android-g++ {

INCLUDEPATH += $$PWD

HEADERS += \
    $$PWD/QAndroidScreenLocker.h \
    $$PWD/QAndroidWiFiLocker.h \
    $$PWD/QLocks/QLockBase.h  \
    $$PWD/QLocks/QLockedObjectBase_p.h  \
    $$PWD/QLocks/QLockedObject.h  \
    $$PWD/QLocks/QLockHandler_p.h  \
    $$PWD/QLocks/QLock_p.h \
    $$PWD/QAndroidConfiguration.h \
    $$PWD/QAndroidDialog.h \
    $$PWD/QAndroidDisplayMetrics.h \
    $$PWD/QAndroidFilePaths.h \
    $$PWD/QAndroidScreenOrientation.h \
    $$PWD/QAndroidScreenLayoutHandler.h \
    $$PWD/QAndroidToast.h \
    $$PWD/QAndroidDesktopUtils.h

SOURCES += \
    $$PWD/QAndroidScreenLocker.cpp \
    $$PWD/QAndroidWiFiLocker.cpp \
    $$PWD/QLocks/QLock.cpp  \
    $$PWD/QLocks/QLockedObject.cpp  \
    $$PWD/QLocks/QLockHandler.cpp \
    $$PWD/QAndroidConfiguration.cpp \
    $$PWD/QAndroidDialog.cpp \
    $$PWD/QAndroidDisplayMetrics.cpp \
    $$PWD/QAndroidFilePaths.cpp \
    $$PWD/QAndroidScreenOrientation.cpp \
    $$PWD/QAndroidScreenLayoutHandler.cpp \
    $$PWD/QAndroidToast.cpp \
    $$PWD/QAndroidDesktopUtils.cpp
}
